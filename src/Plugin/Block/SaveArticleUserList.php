<?php

namespace Drupal\qs_articles\Plugin\Block;

use Drupal;
use Drupal\Core\Block\BlockBase;
use Drupal\user\Entity\User;

/**
 * Provides a 'article' block.
 *
 * @Block(
 *   id = "save_article_user_list",
 *   admin_label = @Translation("Save Article User List"),
 *   category = @Translation("Save Article User List block")
 * )
 */
class SaveArticleUserList extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $lang = Drupal::languageManager()->getCurrentLanguage()->getId();
    $nid = Drupal::routeMatch()->getRawParameter('node');
    $query = \Drupal::database()->select('node_field_data', 'n');
    $query->LeftJoin('flagging', 'f', 'f.entity_id = n.nid');
    $query->Join('users_field_data', 'ud', 'ud.uid = f.uid');
    $query->fields('f', ['flag_id', 'entity_id', 'uid', 'created']);
    $query->fields('n', ['nid', 'type', 'langcode']);
    $query->condition('f.entity_id', $nid);
    $query->condition('f.flag_id', "save_content");
    $query->condition('n.langcode', $lang);
    $query->orderBy('created', 'DESC');
    $query->condition('n.type', "article");
    $user_data = $query->execute()->fetchAll();
    if (!empty($user_data)) {
      $count = count($user_data);
      // Desktop.
      $total_count = $count - 3;
      $total_value = '';
      if ($total_count > 0) {
        $total_value = $total_count;
      }
      if ($total_value > 1000) {
        $total_value = "1k";
      }

      $user_image = [];
      foreach ($user_data as $key => $value) {
        $uid = $value->uid;
        $user = User::load($uid);
        $user_image[$uid] = $user->mail->value;
        if ($key == 2) {
          break;
        }
      }

      // Mobile.
      $total_count_m = $count - 2;
      $total_value_m = '';
      if ($total_count_m > 0) {
        $total_value_m = $total_count_m;
      }
      if ($total_value_m > 1000) {
        $total_value_m = "1k";
      }

    }
    return [
      '#theme' => 'qs_save_article_user_list',
      '#user_image' => isset($user_image) ? $user_image : '',
      '#total_value' => isset($total_value) ? $total_value : '',
      '#total_value_m' => isset($total_value_m) ? $total_value_m : '',
    ];
  }

}
