<?php

namespace Drupal\qs_articles\Plugin\Block;

use Drupal;
use Drupal\Core\Block\BlockBase;
use Drupal\node\Entity\Node;

/**
 * Provides a 'article' block.
 *
 * @Block(
 *   id = "lead_carousel_modal",
 *   admin_label = @Translation("Lead Carousel Modal"),
 *   category = @Translation("Lead Carousel modal block")
 * )
 */
class LeadCarouselModal extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    $carousel_data = [];
    if (Drupal::routeMatch()->getParameter('node')) {
      $nid = '';
      $lang_code = \Drupal::languageManager()->getCurrentLanguage()->getId();
      $nid = Drupal::routeMatch()->getParameter('node');
      $node = Node::load($nid->id());
      if ($node->get('field_article_lead_carousel')->getValue()) {
        foreach ($node->get('field_article_lead_carousel') as $key => $para) {
          $paragraph = $para->entity;
          if ($paragraph->field_type->value) {
            if ($paragraph->field_type->value == 'image') {
              $carousel_data[$key]['type'] = $paragraph->field_type->value;
              if ($paragraph->field_carousel_image->entity) {
                $carousel_data[$key]['image_path'] = file_create_url($paragraph->field_carousel_image->entity->getFileUri());
                $carousel_data[$key]['image_title'] = $paragraph->getTranslation($lang_code)->field_carousel_image->getValue()[0]['title'];
                $carousel_data[$key]['image_alt'] = $paragraph->getTranslation($lang_code)->field_carousel_image->getValue()[0]['alt'];
              }
            }
            else {
              if (preg_match('/\byoutube.com\b/i', $paragraph->field_video_url->value)) {
                $carousel_data[$key]['type'] = $paragraph->field_type->value;
                $carousel_data[$key]['video_title'] = $paragraph->field_video_title->getValue()[0]['value'];
                preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $paragraph->field_video_url->value, $match);
                $carousel_data[$key]['video_id'] = $match[1];
              }

            }
          }
        }
      }
      else {
        $carousel_data = '';
      }
    }

    return [
      '#theme' => 'lead_carousel_modal',
      '#carousel_data' => $carousel_data,
    ];
  }

}
