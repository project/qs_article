<?php

namespace Drupal\qs_articles\Plugin\Block;

use Drupal;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'Sticky Dfp Block' block.
 *
 * @Block(
 *   id = "sticky_dfp_block",
 *   admin_label = @Translation("Sticky Dfp Block"),
 *   category = @Translation("Sticky Dfp Block")
 * )
 */

class StickyDfpBlock extends BlockBase {
  /**
   * {@inheritdoc}
   *
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['position_to_display'] = [
      '#type' => 'select',
      '#title' => $this->t('Select position'),
      '#options' => [
        '' => '-Select-',
        'above_title' => 'Above title',
        //'below_title' => 'Below title',
        'above_gsack_tray' => 'Above gsack tray',
        'below_gsack_tray' => 'Below gsack tray',
      ],
      '#attributes' => [
        ' class' => $config['position_to_display'],
      ],
      '#default_value' =>  isset($config['position_to_display']) ? $config['position_to_display'] : '',
    ];
    $form['type_of_display'] = [
      '#type' => 'select',
      '#title' => $this->t('Select type of display'),
      '#options' => [
        '' => '-None-',
        'time_based' => 'time based',
        'depth_based' => 'Depth based',
      ],
         '#states' => [
        'visible' => [
           'select[name="settings[position_to_display]"]' =>  [
             ['value' => 'above_title'],
           ],
        ],
      ],
     '#default_value'=>isset($config['type_of_display']) ? $config['type_of_display'] : '',
    ];

    $form['time_in_sec'] = [
      '#type' => 'textfield',
      '#attributes' => array(
          ' type' => 'number',
      ),
      '#title' => 'Time In Second',
      '#default_value'=>isset($config['time_in_sec']) ? $config['time_in_sec'] : '',
      '#states' => [
        'visible' => [
            [
              'select[name="settings[type_of_display]"]' => ['value' => 'time_based'],
              'and',
              'select[name="settings[position_to_display]"]' =>  [
                 ['value' => 'above_title'],
               ],
           ],
        ],
      ],
    ];

    $form['scroll_depth'] = [
      '#type' => 'textfield',
      '#attributes' => array(
          ' type' => 'number',
      ),
      '#title' =>  $this->t('Scroll Depth'),
      '#default_value'=>isset($config['scroll_depth']) ? $config['scroll_depth'] : '',
       '#states' => [
        'visible' => [
            [
              'select[name="settings[type_of_display]"]' => ['value' => 'depth_based'],
              'and',
              'select[name="settings[position_to_display]"]' =>  [
                 ['value' => 'above_title'],
               ],
           ],
        ],
      ],
      '#description' => t('Enter depth from 1-90 percent only'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {
    $scroll_depth = $form_state->getValue('scroll_depth');
     if($scroll_depth > 90){
          $form_state->setErrorByName('Max_value', $this->t('Maximum Scroll depth must be less or equal to 90 percent'));
     }
 }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $type_of_display = $form_state->getValue('type_of_display') ;
    $position_to_display = $form_state->getValue('position_to_display');
    $time_in_sec = $form_state->getValue('time_in_sec');
    $scroll_depth = $form_state->getValue('scroll_depth');

     $this->configuration['position_to_display']=$values['position_to_display'];
    $this->configuration['type_of_display'] = $values['type_of_display'] ;
    $this->configuration['time_in_sec'] = $values['time_in_sec'] ;
    $this->configuration['scroll_depth'] = $values['scroll_depth'] ;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $config = $this->getConfiguration();
    $data = [] ;
    if (isset($config['position_to_display']) && !empty($config['position_to_display'])) {
      $data['position_to_display'] = $config['position_to_display'];
    }

    if (isset($config['type_of_display']) && !empty($config['type_of_display'])) {
      $data['type_of_display'] = $config['type_of_display'];
    }


    if (isset($config['time_in_sec']) && !empty($config['time_in_sec'])) {
      $data['time_in_sec'] = $config['time_in_sec'];
    }

    if (isset($config['scroll_depth']) && !empty($config['scroll_depth'])) {
      $data['scroll_depth'] = $config['scroll_depth'];
    }
    if( $data['position_to_display']=='above_gsack_tray'||$data['position_to_display']=='below_gsack_tray' ){
         $data['type_of_display'] ='';
         $data['time_in_sec'] = '';
         $data['scroll_depth'] = '';
    }
    if(!isset($data['type_of_display']) && empty($data['type_of_display'] )){
       $data['time_in_sec'] = '';
       $data['scroll_depth'] = '';
    }
    return [
      '#theme' => 'qs_sticky_dfp_block',
      '#data' => $data,
    ];
  }


}
