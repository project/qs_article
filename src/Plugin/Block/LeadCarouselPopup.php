<?php

namespace Drupal\qs_articles\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'article' block.
 *
 * @Block(
 *   id = "lead_carousel_popup",
 *   admin_label = @Translation("Lead Carousel Popup"),
 *   category = @Translation("Lead Carousel Popup block")
 * )
 */
class LeadCarouselPopup extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#theme' => 'lead_carousel_popup',
    ];
  }

}
