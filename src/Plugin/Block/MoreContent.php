<?php

namespace Drupal\qs_articles\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'More content ' block.
 *
 * @Block(
 *   id = "more_content",
 *   admin_label = @Translation("More content Block"),
 *   category = @Translation("More content Block")
 * )
 */
class MoreContent extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();
    $form['register_free_content'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Register for free'),
      '#description' => $this->t('Enter Register for free content'),
      '#default_value' => isset($config['register_free_content']) ? $config['register_free_content'] : '',
      '#format' => 'panopoly_wysiwyg_text',
      //'#allowed_formats' => ['panopoly_wysiwyg_text'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['register_free_content'] = $values['register_free_content'];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    if (isset($config['register_free_content']) && !empty($config['register_free_content'])) {
      $data['register_free_content'] = $config['register_free_content'];
    }
    //kint($data);
    return [
      '#theme' => 'qs_more_content',
      '#data' => $data,
    ];
  }

}
