<?php

namespace Drupal\qs_articles\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'Article Comment' block.
 *
 * @Block(
 *   id = "artice_leave_comment",
 *   admin_label = @Translation("Article leave comment Block"),
 *   category = @Translation("Article Block")
 * )
 */
class LeaveComment extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['comment_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#description' => $this->t('Comment Block Title'),
      '#default_value' => isset($config['comment_title']) ? $config['comment_title'] : 'Want to leave a comment?',
    ];

    $form['comment_title_china'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#description' => $this->t('Comment Block Title'),
      '#default_value' => isset($config['comment_title_china']) ? $config['comment_title_china'] : 'Want to leave a comment?',
    ];

    $form['comment_description1'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Comment Block Description1'),
      '#description' => $this->t('Comment Block Description1'),
      '#default_value' => isset($config['comment_description1']) ? $config['comment_description1'] : '',
    ];

    $form['comment_description1_china'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Comment Block Description1 for China'),
      '#description' => $this->t('Comment Block Description1 for China'),
      '#default_value' => isset($config['comment_description1_china']) ? $config['comment_description1_china'] : '',
    ];

    $form['comment_description2'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Comment Block Description2'),
      '#description' => $this->t('Comment Block Description2'),
      '#default_value' => isset($config['comment_description2']) ? $config['comment_description2'] : '',
    ];

    $form['comment_description2_china'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Comment Block Description2 for China'),
      '#description' => $this->t('Comment Block Description2 for China'),
      '#default_value' => isset($config['comment_description2_china']) ? $config['comment_description2_china'] : '',
    ];

    $form['join_us'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Join Us Label'),
      '#description' => $this->t('Join Us Button Label'),
      '#default_value' => isset($config['join_us']) ? $config['join_us'] : 'Join us now',
      '#value' => isset($config['join_us']) ? $config['join_us'] : 'Join us now',
    ];

    $form['join_us_china'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Join Us Label China'),
      '#description' => $this->t('Join Us Button Label China'),
      '#default_value' => isset($config['join_us_china']) ? $config['join_us_china'] : 'Join us now',
    ];

    $form['why_join'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Why Join Label'),
      '#description' => $this->t('Why Join Button Label'),
      '#default_value' => isset($config['why_join']) ? $config['why_join'] : 'Why join?',
    ];

    $form['why_join_china'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Why Join Label China'),
      '#description' => $this->t('Why Join Button Label China'),
      '#default_value' => isset($config['why_join_china']) ? $config['why_join_china'] : 'Why join?',
    ];

    $form['why_join_video'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Why Join Video'),
      '#description' => $this->t('Why Join Video'),
      '#default_value' => isset($config['why_join_video']) ? $config['why_join_video'] : '',
    ];

    $form['why_join_video_china'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Why Join Video in China'),
      '#description' => $this->t('Why Join Video in China'),
      '#default_value' => isset($config['why_join_video_china']) ? $config['why_join_video_china'] : '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['comment_title'] = $values['comment_title'];
    $this->configuration['comment_title_china'] = $values['comment_title_china'];
    $this->configuration['comment_description1'] = $values['comment_description1'];
    $this->configuration['comment_description1_china'] = $values['comment_description1_china'];
    $this->configuration['comment_description2'] = $values['comment_description2'];
    $this->configuration['comment_description2_china'] = $values['comment_description2_china'];
    $this->configuration['join_us'] = $values['join_us'];
    $this->configuration['join_us_china'] = $values['join_us_china'];
    $this->configuration['why_join'] = $values['why_join'];
    $this->configuration['why_join_china'] = $values['why_join_china'];
    $this->configuration['why_join_video'] = $values['why_join_video'];
    $this->configuration['why_join_video_china'] = $values['why_join_video_china'];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    $data['comment_title'] = $config['comment_title'];
    $data['comment_title_china'] = $config['comment_title_china'];
    $data['site_name'] = \Drupal::config('system.site')->get('name');
    if (isset($config['comment_description1']) && !empty($config['comment_description1'])) {
      $data['comment_description1'] = $config['comment_description1'];
    }
    if (isset($config['comment_description1_china']) && !empty($config['comment_description1_china'])) {
      $data['comment_description1_china'] = $config['comment_description1_china'];
    }
    if (isset($config['comment_description2']) && !empty($config['comment_description2'])) {
      $data['comment_description2'] = $config['comment_description2'];
    }
    if (isset($config['comment_description2_china']) && !empty($config['comment_description2_china'])) {
      $data['comment_description2_china'] = $config['comment_description2_china'];
    }
    $data['join_us'] = $config['join_us'];
    $data['join_us_china'] = $config['join_us_china'];
    $data['why_join'] = $config['why_join'];
    $data['why_join_china'] = $config['why_join_china'];
    if (isset($config['why_join_video']) && !empty($config['why_join_video'])) {
      $data['why_join_video'] = $config['why_join_video'];
    }
    if (isset($config['why_join_video_china']) && !empty($config['why_join_video_china'])) {
      $data['why_join_video_china'] = $config['why_join_video_china'];
    }
    //echo "<pre>";print_r($data);
    return [
      '#theme' => 'qs_leave_comment',
      '#data' => $data,
    ];
  }

}
