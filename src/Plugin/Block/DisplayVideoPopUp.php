<?php

namespace Drupal\qs_articles\Plugin\Block;

use Drupal;
use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'DisplayVideo' block.
 *
 * @Block(
 *   id = "Display_video_popup",
 *   admin_label = @Translation("Display Video Popup"),
 *   category = @Translation("Display Video Popup block")
 * )
 */
class DisplayVideoPopUp extends BlockBase {
   /**
   * {@inheritdoc}
   */
    public function build() {
        return [
            '#theme' => 'qs_display_video_popup',
        ];
    }
}
