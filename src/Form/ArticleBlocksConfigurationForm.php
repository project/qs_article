<?php

namespace Drupal\qs_articles\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form that configures forms batch size settings.
 */
class ArticleBlocksConfigurationForm extends ConfigFormBase
{

    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'article_blocks_configuration_settings_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $config = $this->config('qs_articleblocks.adminsettings');
        $vid = 'categories';
        $terms =\Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
        $term_data['0'] = 'None';
        foreach ($terms as $term) {
            $term_data[$term->tid] = $term->name ;
        }

        $form['article_cat'] = [
        '#type' => 'select',
        '#target_type' => 'taxonomy_term',
        '#title' => $this->t('Select Category'),
        '#default_value' => ($config->get('article_cat') ? $config->get('article_cat') : ''),
        '#options' =>$term_data,
        '#weight' => '0',
        ];

        $form['date_range'] = [
        '#type' => 'select',
        '#title' => $this->t('Select date range'),
        '#options' => [
        '30' => $this->t('Last month'),
        '90' => $this->t('Last 3 months'),
        '180' => $this->t('Last 6 months'),
        '365' => $this->t('Last year'),
        ],
        '#default_value' => ($config->get('date_range') ? $config->get('date_range') : ''),
        ];
        return parent::buildForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $date_range = $form_state->getValue('date_range');
        $art_cat = $form_state->getValue('article_cat');
        parent::submitForm($form, $form_state);

        $this->config('qs_articleblocks.adminsettings')
            ->set('date_range', $date_range)
            ->set('article_cat', $art_cat)
            ->save();
    }

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames()
    {
        return [
        'qs_articleblocks.adminsettings',
        ];
    }
}
