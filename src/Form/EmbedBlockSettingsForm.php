<?php

namespace Drupal\qs_articles\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form that configures forms batch size settings.
 */
class EmbedBlockSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'embed_block_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('qs_articles.embedblockssettings');

    // Get list of all custom blocks, excluding few.
    $blockManager = \Drupal::service('plugin.manager.block');
    $contextRepository = \Drupal::service('context.repository');
    // Get blocks definition
    $definitions = $blockManager->getFilteredDefinitions('block_ui', $contextRepository->getAvailableContexts());

    foreach ($definitions as $key => $value) {
      //$blocklist[''] = $this->t('- None -');
      if(strpos($key, "dfp_ad:") === 0 || $key == 'block_content:d5db902c-f544-4c52-a86c-0698c0de78ff' || $key == 'block_content:df710696-939c-415b-862e-4b7f504c9ade' || $key == 'block_content:13a5cdae-74c3-4989-ad0a-24be9053a4b2') {
          if($key != 'dfp_ad:07db1a2d-aea8-4679-82e6-3b61b5b19f63' && $key != 'dfp_ad:9a996462-038f-4b02-ba07-6a407525e94d' && $key != 'dfp_ad:47082ba0-74f2-4887-9290-017eaf643164' && $key != 'dfp_ad:f36080cf-3c3c-4c4b-a621-9a945a2ddd42' && $key != 'dfp_ad:d147740e-e986-440c-abac-d6b6b4e3b924' && $key != 'dfp_ad:7a31abd0-a155-4da3-bf1c-197b66b5d11f' && $key != 'dfp_ad:2c5a05bf-567b-4d58-ac2f-01d246dd6dea' && $key != 'dfp_ad:4b163f9a-47a2-4ab1-84f5-6c62eebbd975') {
            if(is_object($value['admin_label'])){
              $blocklist[$key] = $value['admin_label']->__toString();
            }else{
              $blocklist[$key] = $value['admin_label'];
            }
          }
      }
    }

    $blocklist_ranking = $blocklist;
    // Remove Responsive ad 4 as its already in layout.
    unset($blocklist_ranking['dfp_ad:ada3bfc6-0fac-42a8-a540-0b24788fbd67']);

    $form['ranking_block_embed'] = array(
      '#type' => 'details',
      '#title' => $this->t('Blocks to be embed in ranking tables'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE
    );

    $form['ranking_block_embed']['custom_block_ranking'] = [
      '#type' => 'select',
      '#options' => $blocklist_ranking,
      '#title' => $this->t('1st Block to embed in Ranking table'),
      '#multiple' => TRUE,
      '#size' => 13,
      '#default_value' => ($config->get('custom_block_ranking') ? $config->get('custom_block_ranking') : ''),
      '#attributes' => [
        'class' => ['chosen_select_max_3'],
      ],
    ];

    $form['ranking_block_embed']['ranking_record_number'] = [
      '#type' => 'select',
      '#title' => $this->t('Select row after which 1st block will be embeded'),
      '#options' => [
        '' => $this->t('-Select-'),
        '1' => '1',
        '2' => '2',
        '3' => '3',
        '4' => '4',
        '5' => '5',
        '6' => '6',
        '7' => '7',
        '8' => '8',
        '9' => '9',
        '10' => '10',
      ],
      '#default_value' => ($config->get('ranking_record_number') ? $config->get('ranking_record_number') : ''),
    ];

    $form['ranking_block_embed']['custom_block_ranking_2'] = [
      '#type' => 'select',
      '#options' => $blocklist_ranking,
      '#title' => $this->t('2nd Block to embed in Ranking table'),
      '#multiple' => TRUE,
      '#size' => 13,
      '#default_value' => ($config->get('custom_block_ranking_2') ? $config->get('custom_block_ranking_2') : ''),
      '#description' => t('Select DFP other then what is selected for 1st block.'),
      '#attributes' => [
        'class' => ['chosen_select_max_3'],
      ],
    ];

    $form['ranking_block_embed']['ranking_record_number_2'] = [
      '#type' => 'select',
      '#title' => $this->t('Select row after which 2nd block will be embeded'),
      '#options' => [
        '' => '-Select-',
        '1' => '1',
        '2' => '2',
        '3' => '3',
        '4' => '4',
        '5' => '5',
        '6' => '6',
        '7' => '7',
        '8' => '8',
        '9' => '9',
        '10' => '10',
      ],
      '#default_value' => ($config->get('ranking_record_number_2') ? $config->get('ranking_record_number_2') : ''),
    ];

    $form['block_embed_paragraph'] = array(
      '#type' => 'details',
      '#title' => $this->t('Blocks to be embed in articles paragraph'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE
    );

    // Remove Article Advertisement 1, 2 as its already in layout.
    unset($blocklist['dfp_ad:c7043dc6-cdc6-4734-8de5-d005fc57f192']);
    unset($blocklist['dfp_ad:02e6d642-560b-4c8e-8a49-6695822d3186']);

    $form['block_embed_paragraph']['custom_block_para'] = [
      '#type' => 'select',
      '#options' => $blocklist,
      '#title' => $this->t('Block to embed in paragraph'),
      '#multiple' => TRUE,
      '#size' => 13,
      '#default_value' => ($config->get('custom_block_para') ? $config->get('custom_block_para') : ''),
      '#attributes' => [
        'class' => ['chosen_select_max_2'],
      ],
    ];

    $form['block_embed_paragraph']['paragraph_number'] = [
      '#type' => 'select',
      '#title' => $this->t('Select paragraph number after which block will be embeded'),
      '#options' => [
        '' => $this->t('-Select-'),
        '1' => '1',
        '2' => '2',
        '3' => '3',
        '4' => '4',
        '5' => '5',
        '6' => '6',
        '7' => '7',
        '8' => '8',
        '9' => '9',
        '10' => '10',
      ],
      '#default_value' => ($config->get('paragraph_number') ? $config->get('paragraph_number') : ''),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
     $rbcount = $form_state->getValue('custom_block_ranking');
     if(count($rbcount) > 3){
          $form_state->setErrorByName('Max_value', $this->t('Maximum 3 value can be selected for Ranking table.'));
     }
     $cbcount = $form_state->getValue('custom_block_para');
     if(count($cbcount) > 3){
          $form_state->setErrorByName('Max_value', $this->t('Maximum 3 value can be selected to embed in paragraph.'));
     }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $custom_block_ranking = array_filter($form_state->getValue('custom_block_ranking'));
    $ranking_record_number = $form_state->getValue('ranking_record_number') ? $form_state->getValue('ranking_record_number') : '3';
    $custom_block_ranking_2 = array_filter($form_state->getValue('custom_block_ranking_2'));
    $ranking_record_number_2 = $form_state->getValue('ranking_record_number_2') ? $form_state->getValue('ranking_record_number_2') : '7';
    $custom_block_para = array_filter($form_state->getValue('custom_block_para'));
    $paragraph_number = $form_state->getValue('paragraph_number') ? $form_state->getValue('paragraph_number') : '1';
    parent::submitForm($form, $form_state);
    $this->config('qs_articles.embedblockssettings')
      ->set('custom_block_ranking', $custom_block_ranking)
      ->set('ranking_record_number', $ranking_record_number)
      ->set('custom_block_ranking_2', $custom_block_ranking_2)
      ->set('ranking_record_number_2', $ranking_record_number_2)
      ->set('custom_block_para', $custom_block_para)
      ->set('paragraph_number', $paragraph_number)
      ->save();
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'qs_articles.embedblockssettings',
    ];
  }

}
